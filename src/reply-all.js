// ==UserScript==
// @name         4chan X Reply-All
// @namespace    http://tampermonkey.net/
// @version      0.1.0
// @description  Adds buttons so you can easily reply-all on 4chan at the thread- or post-level.
// @author       Luna Lucadou
// @homepage     https://luna.lucadou.sh/
// @source       https://gitlab.com/lucadou/4chan-reply-all
// @updateURL    https://gitlab.com/lucadou/4chan-reply-all/-/raw/master/src/reply-all.js
// @downloadURL  https://gitlab.com/lucadou/4chan-reply-all/-/raw/master/src/reply-all.js
// @include      https://boards.4chan.org/*
// @include      https://boards.4channel.org/*
// @grant        none
// ==/UserScript==

(function(){
    'use strict';

    /* Global variables */
    let replyAllCtrClass = 'reply-all-ctr';
    let replyAllCtrStyle = 'padding-left: 0.25rem';
    let replyAllBtnClass = 'reply-all-btn';
    let replyAllBtnText = '&gt;&gt;Reply-All';
    let replyCCJoiner = ', ';

    let sleep = function(ms) {
        // Adapted from https://www.sitepoint.com/delay-sleep-pause-wait/#abettersleepfunction
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    /* Thread-level reply-all */
    /*let addReplyFormButton = function(quickReplyPrompt) {
        let goodMorningBtn = document.createElement("button");
        let goodMorningBtnText = document.createTextNode("Reply-All");

        goodMorningBtn.onclick = function() {
            let arr = Array.from(document.querySelectorAll(".postContainer")).map(
                e=>{ return ">>" + e.id.slice(2) }
            );
            document.querySelector('div.textarea > textarea[placeholder="Comment"]').value =
                arr.slice(Math.max(arr.length - 150, 0)).join(replyCCJoiner) + "\n";
        }
        goodMorningBtn.appendChild(goodMorningBtnText);
        quickReplyPrompt.appendChild(goodMorningBtn);
    }

    let checkQuickReplyexists = new MutationObserver(function(mutationList, observer) {
        let quickReplyPrompt = document.getElementById('qr');
        if (quickReplyPrompt) {
            addReplyFormButton(quickReplyPrompt);
            observer.disconnect();
            return;
        }
    }).observe(document, { childList: true, subtree: true });
    // chidList - Set to true if mutations to target’s children are to be observed
    // subtree - Set to true if mutations to target's descendants are to be observed
    // https://dom.spec.whatwg.org/#ref-for-dom-mutationobserverinit-subtree%E2%91%A1

    /* Post-level reply-all */
    let replyAllPost = function(targetPost) {
        // Fetch post
        let post = document.getElementById('pi' + targetPost);
        let postReplies = post.querySelector('span.container:not(.' + replyAllCtrClass + ')').children
        let toPost = '>>' + targetPost;
        let ccPosts = [];
        for (let i = 0; i < postReplies.length; i++) {
            let postReply = postReplies[i];
            ccPosts.push(postReply.innerText); // Already contain ">>", no need to prepend it
        }

        // Add to the quick reply prompt
        // Ensure quick reply prompt is present
        if (document.getElementById('qr') === null) {
            // Quick reply prompt has not yet been opened; open it
            document.getElementById('shortcut-qr').children[0].click();
        }
        // Quick reply prompt has been opened; force it to be shown
        let qrPrompt = document.getElementById('qr');
        qrPrompt.removeAttribute('hidden');
        // If already visible, ^ does nothing
        let qrPromptTextbox = qrPrompt.querySelector('div.textarea > textarea[placeholder="Comment"]');
        qrPromptTextbox.value += 'To: ' + toPost + '\n';
        if (ccPosts.length > 0) {
            qrPromptTextbox.value += 'CC: ' + ccPosts.join(replyCCJoiner) + '\n';
        }
    }

    sleep(2000).then(() => {

        // Add Reply-All button to posts
        let addReplyAllButtons = function() {
            /* TODO: optimize the selector when CSS4 browser support improves
         *
         * I should use a selector which excludes posts with the button already present,
         * such as:
         * 'div.postContainer:not(:has(div.postInfo > span.container.reply-all-ctr))'
         *
         * This is not yet possible without importing jQuery (which has had it for >10 years),
         * as the :has CSS4 selector is not yet widely supported.
         * https://drafts.csswg.org/selectors-4/#relational
         * https://caniuse.com/css-has
         * However, in the future, this will be revisited.
         */
            let posts = document.querySelectorAll('div.postContainer');
            let btnsAdded = 0;
            for (let i = 0; i < posts.length; i++) {
                let currentPost = posts[i];
                let postInfo = currentPost.querySelector('div.postInfo');
                // Make sure it's not already present
                if (!postInfo.children[postInfo.children.length - 1].classList.contains(replyAllCtrClass)) {
                    btnsAdded++;
                    let replyAllCtr = document.createElement('span');
                    replyAllCtr.classList.add('container');
                    replyAllCtr.classList.add(replyAllCtrClass);
                    //replyAllCtr.style = replyAllCtrStyle;

                    let replyAllBtn = document.createElement('a');
                    replyAllBtn.classList.add('backlink');
                    replyAllBtn.classList.add(replyAllBtnClass);
                    replyAllBtn.href = 'javascript:;'
                    replyAllBtn.setAttribute('role', 'button');
                    replyAllBtn.innerHTML = replyAllBtnText;
                    replyAllBtn.addEventListener('click', event => {
                        replyAllPost(postInfo.id.substring(2));
                        // ID has "pi" prepended, have to remove that first
                        console.log('Replying all:');
                        console.log(document.querySelector('div.textarea > textarea[placeholder="Comment"]').value);
                    });
                    replyAllCtr.appendChild(replyAllBtn);
                    postInfo.appendChild(replyAllCtr);
                }
            }
            console.log(btnsAdded + ' Reply-All button(s) added.');
        }

        console.log('Initial page Reply-All button adding in progress...');
        addReplyAllButtons();

        // Watch for new posts
        let observer = new MutationObserver(function(mutationList, observer) {
            console.log('Thread updated, adding new Reply-All buttons...');
            addReplyAllButtons();
        }).observe(document.querySelector('div.board div.thread'), {childList: true});
        // chidList - Set to true if mutations to target’s children are to be observed
        // https://dom.spec.whatwg.org/#interface-mutationobserver
    });

    /* This script operates by checking every post each time the thread is updated.
     * This may seem inefficient, but when the thread is first loaded, the first run
     * can skip some posts, especially if the thread has many posts in it.
     * As such, it is necessary to re-check all posts each time the thread updates
     * to ensure none are missed.
     */
})();
