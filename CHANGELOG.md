# Changelog
All notable changes to this project will be documented in this file.

## [1.0.0] - 2022-??-??

### Changed

- Fixed 4chan X post nesting issues

## [0.1.0] - 2022-06-06

### Added

- Basic script
- Changelog
- License
- Readme

