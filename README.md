# 4chan X Reply-All

Have you ever wanted to send email-style reply-all messages on 4chan?

Of course not!
But now you can.

This userscript allows for replying to a specific post and all of its
responders or the entire thread.

(Need demo image/video here)

### Table of Contents

* [Getting Started](#getting-started)
  * [Requirements](#requirements)
  * [Installation](#installation)
  * [Usage](#usage)
* [Notes/FAQ](#notesfaq)
* [Roadmap](#roadmap)
* [Versioning](#versioning)
* [Contributing](#contributing)
* [Authors and Acknowledgments](#authors-and-acknowledgments)
* [License](#license)

## Setup

### Requirements

To run this, you must have:

* A modern web browser
* A userscript extension such as
  [Violentmonkey](https://violentmonkey.github.io/),
  [Greasemonkey](https://www.greasespot.net/),
  or [Tampermonkey](https://www.tampermonkey.net/)
* The [4chan X userscript or extension](https://github.com/ccd0/4chan-x#install)

### Installation



### Usage



## Notes/FAQ

(Need to put inspiration here.)

* https://desuarchive.org/g/thread/87233068/#87233865
* https://desuarchive.org/g/thread/87233068/#87234913

(Need to create a logo for the project and write what I did here.)

## Roadmap

When CSS4 becomes more widely supported, particularly the
[`:has` selector](https://caniuse.com/css-has),
some efficiency gains can be realized when processing new posts.

## Versioning

This project uses Semantic Versioning, aka
[SemVer](https://semver.org/spec/v2.0.0.html).

## Contributing

If you would like to suggest features, report bugs, etc., feel free to open an
issue.

If you would like to help develop the application, merge requests are welcome.
For major changes, please open an issue first to discuss what you would like
to change.

## Authors and Acknowledgments

* Anonymous - Wrote the
  [original script](https://desuarchive.org/g/thread/87233068/#87234913)
  and posted [a demo](https://desuarchive.org/g/thread/87233068/#87233865)
  to /g/
* Luna Lucadou - Reworked and expanded the script in her free time

## License

This project is licensed under the GNU General Public License v3.0 - see the
[LICENSE.md](LICENSE.md) file for details.

